package iut.assoloc;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
public class Adherent {
	private int nbEmpruntMax = 10;
	private int id;
	private String nom, adresse;
	private int[] objetEmprunteIds;
	
	public int getNbEmpruntMax() {
		return nbEmpruntMax;
	}
	public void setNbEmpruntMax(int nbEmpruntMax) {
		this.nbEmpruntMax = nbEmpruntMax;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public int[] getObjetEmprunteIds() {
		return objetEmprunteIds;
	}
	public void setObjetEmprunteIds(int[] objetEmprunteIds) {
		this.objetEmprunteIds = objetEmprunteIds;
	}
	public Adherent(){
		this.objetEmprunteIds=new int[nbEmpruntMax];
		for(int i=0; i<this.objetEmprunteIds.length;i++){
			this.objetEmprunteIds[i]=0;
		}
	}
	public String toString(){
		String _s= getNom()+"("+getId()+")"+" Adresse: "+getAdresse()+", emprunts: "+getNombreEmprunts()+" [";
		for(int i=0; i<getNombreEmprunts();i++){
			if(i!=0) _s+=", ";
			_s+=this.objetEmprunteIds[i];
		}
		return _s+"]";
	}

	public int getNombreEmprunts(){
		int _nb=0;
		for(int i=0; i<this.objetEmprunteIds.length;i++){
			if(objetEmprunteIds[i]!=0) _nb++;
		}
		return _nb;
	}
	public boolean ajouterObjetEmprunte(int objetId){
		if(getNombreEmprunts()<nbEmpruntMax){
			this.objetEmprunteIds[getNombreEmprunts()]=objetId;
			return true;
		}
		return false;
	}
	public boolean rendreObjectEmprunte(int objetId){
		int _nbEmprunt=this.getNombreEmprunts();
		for(int i=0; i<this.getNombreEmprunts();i++){
			if(objetEmprunteIds[i]==objetId) {
				//l'index du dernier id est this.getNombreEmprunts()-1
				if(i<this.getNombreEmprunts()-1){
					objetEmprunteIds[i]=objetEmprunteIds[this.getNombreEmprunts()-1];
				} 
				objetEmprunteIds[this.getNombreEmprunts()-1]=0;
				return true;
			}
		}
		return false;
	}
	public void save(DataOutputStream dos) throws IOException {
		dos.writeInt(getId());
		dos.writeUTF(getNom());
		dos.writeUTF(getAdresse());
		for(int i=0; i<this.objetEmprunteIds.length;i++){
			dos.writeInt(objetEmprunteIds[i]);
		}

	}
	public void load(DataInputStream dis) throws IOException {
		setId(dis.readInt());
		setNom(dis.readUTF());
		setAdresse(dis.readUTF());
		for(int i=0; i<this.objetEmprunteIds.length;i++){
			this.objetEmprunteIds[i]=dis.readInt();
		}

	}
	public static void main(String[] argv)throws IOException{
		String nomFichier = "donnees/tests/adherent.bin";
		Adherent adherent= new Adherent();
		adherent.setNbEmpruntMax(5);
		adherent.setId(10);
		adherent.setNom("Le nom");
		adherent.setAdresse("L'adresse");
		//les ids commencent � 1
		for(int i=1;i<6;i++) {
			adherent.ajouterObjetEmprunte(i);
		}
		System.out.println(adherent);
		System.out.println("l'adherent rend l'objet "+3);
		adherent.rendreObjectEmprunte(3);
		System.out.println(adherent);
		System.out.println("l'adherent emprunte l'objet "+10);
		adherent.ajouterObjetEmprunte(10);
		System.out.println(adherent);
		System.out.println("l'adherent rend l'objet "+3);
		adherent.rendreObjectEmprunte(3);
		System.out.println("Sauve des donnees dans le fichier : "+nomFichier);
		DataOutputStream fluxEcritureAdherents= new DataOutputStream(new BufferedOutputStream(new FileOutputStream(nomFichier)));
		adherent.save(fluxEcritureAdherents);
		fluxEcritureAdherents.close();
		System.out.println("Chargement des donnees a partir du fichier : "+nomFichier);
		DataInputStream fluxLectureAdherents= new DataInputStream(new BufferedInputStream(new FileInputStream(nomFichier)));
		Adherent nouvelAdherent = new Adherent();
		nouvelAdherent.load(fluxLectureAdherents);
		System.out.println(nouvelAdherent);
		fluxLectureAdherents.close();
		
	}
	
	

}
