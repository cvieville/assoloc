package iut.assoloc.tests;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import iut.assoloc.Adherent;
public class TestAdherent {
	private static int nbEmpruntMax = 5;
	private static int nbAdherentMax = 100;
	private static String nomFichier = "donnees/tests/adherents.bin";
	private Adherent[] adherents;
	
	
	public TestAdherent() {
		this.adherents=new Adherent[nbAdherentMax];
		for(int i=0; i<this.adherents.length;i++){
			this.adherents[i]=null;
		}
	}

	public static int getNbEmpruntMax() {
		return nbEmpruntMax;
	}

	public static void setNbEmpruntMax(int nbEmpruntMax) {
		TestAdherent.nbEmpruntMax = nbEmpruntMax;
	}

	public static int getNbAdherentMax() {
		return nbAdherentMax;
	}

	public static void setNbAdherentMax(int nbAdherentMax) {
		TestAdherent.nbAdherentMax = nbAdherentMax;
	}

	public static String getNomFichier() {
		return nomFichier;
	}

	public static void setNomFichier(String nomFichier) {
		TestAdherent.nomFichier = nomFichier;
	}

	public Adherent[] getAdherents() {
		return adherents;
	}

	public void setAdherents(Adherent[] adherents) {
		this.adherents = adherents;
	}
	public int getNombreItems(){
		int _nb=0;
		for(int i=0; i<getAdherents().length;i++){
			if(getAdherents()[i]!=null) _nb++;
		}
		return _nb;
	}
	public void save() throws IOException {
		DataOutputStream _adherents= new DataOutputStream(new BufferedOutputStream(new FileOutputStream(nomFichier)));
		
		_adherents.writeInt(nbAdherentMax);
		_adherents.writeInt(getNombreItems());
		for(int i=0; i<getNombreItems();i++){
			this.adherents[i].save(_adherents);
		}
		_adherents.close();

	}
	public void load() throws IOException {
		DataInputStream _adherents= new DataInputStream(new BufferedInputStream(new FileInputStream(nomFichier)));
		
		nbAdherentMax= _adherents.readInt();
		int nb = _adherents.readInt();
		this.adherents=new Adherent[nbAdherentMax];
		for(int i=0; i<nb; i++){
			Adherent adherent = new Adherent();
			adherent.load(_adherents);
			this.adherents[i]=adherent;
		}
		_adherents.close();

	}
	public Adherent createAdherent(int id){
		Adherent adherent = new Adherent();
		adherent.setId(id);
		adherent.setNom("Nom-"+id);
		adherent.setAdresse("Adresse_"+id);
		adherent.setNbEmpruntMax(5);
		return adherent;
	}
	public String toString(){
		String _s="NbAdherents="+getNombreItems()+" [";
		for(int i=0; i<getNombreItems(); i++){
			if(i>0) _s+=", ";
			_s+=getAdherents()[i].toString();		
		}
		return _s;
	}
	public static void main(String[] argv)throws IOException {
		TestAdherent ta= new TestAdherent();
		for(int i=1; i<nbAdherentMax+1;i++){
			ta.getAdherents()[i-1]=ta.createAdherent(i);
		}
		ta.save();
		TestAdherent tb= new TestAdherent();
		tb.load();
		System.out.println(tb);
		
	}
	
	

}
